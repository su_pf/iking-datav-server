<!--
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-31 11:12:39
 * @LastEditTime : 2024-02-23 18:20:21
-->

<div align="center">
  <a href="https://datav.ikingtech.com/">
    <img src="https://datav.ikingtech.com/logo.png" alt="Logo" width="120" height="120">
  </a>

<h3>金合可视化平台-后端服务（Java版1.0.3）</h3>

<a href="http://www.ikingtech.com/">官网</a>
|
<a href="https://datavdoc.ikingtech.com">文档</a>
|
<a href="https://datav.ikingtech.com">在线演示</a>
|
<a href="https://gitee.com/ikingtech/iking-datav">Web前端</a>
|
<a href="https://gitee.com/ikingtech/iking-datav-server/issues">提交Bug</a>

演示环境账号：admin 密码：Security123$%^
</div>

## 简介

金合可视化平台服务端主要为前端提供数据存储、数据库访问、文件服务、请求代理等能力。 相关文档正在奋力编写中

## 技术栈

* 💪 jdk17
* ⚡ MySQL
* 🍍 Minio
* 🔥 Redis
* 🔥 Maven
* 🔥 Spring Boot

## 使用说明

1. 开发环境参考👀：<br />
   | 环境                 |  版本                         |
      |--------------------|----------------------------|
   | jdk17                    | openjdk-17.0.2                     |
   | mysql         | 8.3.0                    |
   | redis         | 7.2.4                    |
   | maven           | apache-maven-3.3.9 （配置阿里云镜像）     |
   | minio         | RELEASE.2024-01-31T20-20-33Z     |
2. 下载代码，切换到`java`分支。

```
git clone https://gitee.com/ikingtech/iking-datav-server.git
```
3. 使用idea打开下载的项目，配置运行环境jdk17.0.2，配置maven并拉取依赖。
4. 创建数据库`iking_framework_datav`；并确保库内没有其他表(防止初始化表结构错误)。
5. 编译通过后，打开配置文件，配置对应环境变量。<br />
   💡环境变量的说明请参考以下列表：

   | 变量                 | 说明                         |
      |--------------------|----------------------------|
   | SRV_PORT           | 服务运行端口                     |
   | MYSQL_HOST         | 数据库服务地址                    |
   | MYSQL_PORT         | 数据库服务端口                    |
   | MYSQL_SCHEMA_NAME  | 数据库名，默认iking_framework_datav     |
   | MYSQL_USERNAME     | 数据库服务用户名                   |
   | MYSQL_PASSWORD     | 数据库服务密码                    |
   | REDIS_HOST         | Redis服务地址                  |
   | REDIS_PORT         | Redis服务端口                  |
   | REDIS_DB           | Redis DB号                  |
   | REDIS_PASSWORD     | Redis密码                    |
   | OSS_ENABLED        | 是否启用文件服务                   |
   | OSS_STORAGE_TYPE   | 对象存储服务类型，目前仅支持AWS_S3类型     |
   | OSS_HOST           | 对象存储服务地址，格式为http://ip:port |
   | OSS_ACCESS_KEY     | 对象存储服务AccessKey            |
   | OSS_SECRET_KEY     | 对象存储服务SecretKey            |
   | OSS_DEFAULT_BUCKET | 默认桶名称                      |
   | OSS_REGION         | 桶所在区域标识                    |
   |DATAV_FILE_IP      | 可视化资源地址默认设置为minio的9000   |

6. 启动项目，控制台打印`资源初始化完成!!!`，代表运行成功。 <br />
   注意：如需要重新初始化静态资源，可以使用工具连接redis，删除名为`RESOURCE_READY_COMPLETE`的键重启即可。


### 平台截图

![](https://cdn.o0o0oo.com/ikingtech/datav/docPic/%E5%9B%BE%E7%89%872.png#id=hKx7Z&originHeight=908&originWidth=1911&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=)

![](https://cdn.o0o0oo.com/ikingtech/datav/docPic/%E5%9B%BE%E7%89%871.png#id=Z4Guc&originHeight=907&originWidth=1916&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=)

## 技术交流群

![](https://cdn.o0o0oo.com/ikingtech/datav/docPic/%E5%8A%A0%E5%85%A5%E7%BE%A4%E8%81%8A%E4%BA%8C%E7%BB%B4%E7%A0%81.png#height=194&id=dllFA&originHeight=225&originWidth=232&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=&width=200)
<br />
邮箱 - nanawfl@163.com / wfl.118@qq.com
