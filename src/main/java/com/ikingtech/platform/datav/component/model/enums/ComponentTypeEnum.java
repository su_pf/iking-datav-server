package com.ikingtech.platform.datav.component.model.enums;

import lombok.RequiredArgsConstructor;

/**
 * @author fucb
 */
@RequiredArgsConstructor
public enum ComponentTypeEnum {

    /**
     * 普通组件
     */
    COMMON("普通组件"),

    /**
     * 收藏组件
     */
    COLLECT("收藏组件"),

    /**
     * 模板组件
     */
    TEMPLATE("模板组件"),
    ;

    public final String value;

}
