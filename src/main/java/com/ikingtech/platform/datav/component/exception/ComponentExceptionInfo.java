package com.ikingtech.platform.datav.component.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author fucb
 */

@RequiredArgsConstructor
public enum ComponentExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 组件不存在
     */
    COMPONENT_NOT_FOUND("componentNotFound"),
    ;

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-maintenace-datav";
    }
}
