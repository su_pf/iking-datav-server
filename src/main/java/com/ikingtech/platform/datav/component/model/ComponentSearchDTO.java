package com.ikingtech.platform.datav.component.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 组件表(ComponentDO)对象
 *
 * @author fucb
 * @since 2024-02-22 09:34:37
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ComponentSearchDTO对象", description = "对象")
public class ComponentSearchDTO extends PageParam {


    @Schema(name = "id", description = "${column.comment}")
    private String id;

    /**
     * 大屏ID
     */
    @Schema(name = "screenId", description = "大屏ID")
    private String screenId;

    /**
     * 排序序号
     */
    @Schema(name = "sortOrder", description = "排序序号")
    private Integer sortOrder;

    /**
     * 类型
     */
    @Schema(name = "type", description = "类型")
    private String type;

    /**
     * 别名
     */
    @Schema(name = "alias", description = "别名")
    private String alias;

    /**
     * 名称
     */
    @Schema(name = "name", description = "名称")
    private String name;

    /**
     * 子组件
     */
    @Schema(name = "children", description = "子组件")
    private List<Object> children;

    /**
     * 父级ID
     */
    @Schema(name = "parentId", description = "父级ID")
    private String parentId;

    /**
     * 是否隐藏
     */
    @Schema(name = "hided", description = "是否隐藏")
    private Boolean hided;

    /**
     * 图标
     */
    @Schema(name = "icon", description = "图标")
    private String icon;

    /**
     * 图片
     */
    @Schema(name = "img", description = "图片")
    private String img;

    /**
     * 属性
     */
    @Schema(name = "attr", description = "属性")
    private String attr;

    /**
     * 配置
     */
    @Schema(name = "config", description = "配置")
    private String config;

    /**
     * API配置
     */
    @Schema(name = "apis", description = "API配置")
    private String apis;

    /**
     * API数据
     */
    @Schema(name = "apiData", description = "API数据")
    private String apiData;

    /**
     * 入场动画
     */
    @Schema(name = "animate", description = "入场动画")
    private String animate;

    /**
     * 背景图
     */
    @Schema(name = "bgImg", description = "背景图")
    private String bgImg;

    /**
     * 组内配置
     */
    @Schema(name = "scaling", description = "组内配置")
    private String scaling;

    /**
     * 事件
     */
    @Schema(name = "events", description = "事件")
    private String events;


    /**
     * 是否悬停
     */
    @Schema(name = "hovered", description = "是否悬停")
    private Boolean hovered;

    /**
     * 背景动画
     */
    @Schema(name = "bgAnimation", description = "背景动画")
    private String bgAnimation;

    /**
     * 弹窗配置
     */
    @Schema(name = "dialog", description = "弹窗配置")
    private String dialog;

    /**
     * 是否为弹窗
     */
    @Schema(name = "isDialog", description = "是否为弹窗")
    private Integer isDialog;

    /**
     * 版本
     */
    @Schema(name = "version", description = "版本")
    private Integer version;

    /**
     * 是否为公共收藏
     */
    @Schema(name = "isPublic", description = "是否为公共收藏")
    private Integer isPublic;

    /**
     * 项目ID
     */
    @Schema(name = "projectId", description = "项目ID")
    private String projectId;

    /**
     * 模板ID
     */
    @Schema(name = "templateId", description = "模板ID")
    private String templateId;

    /**
     * 响应事件
     */
    @Schema(name = "disActions", description = "响应事件")
    private String disActions;

    /**
     * 组件类型（普通组件，收藏组件，模板组件）
     */
    @Schema(name = "componentType", description = "组件类型（普通组件，收藏组件，模板组件）")
    private String componentType;

    @Schema(name = "createTime", description = "${column.comment}")
    private Date createTime;

    @Schema(name = "updateBy", description = "${column.comment}")
    private String updateBy;

    @Schema(name = "createBy", description = "${column.comment}")
    private String createBy;

    @Schema(name = "createName", description = "${column.comment}")
    private String createName;

    @Schema(name = "updateName", description = "${column.comment}")
    private String updateName;

    @Schema(name = "updateTime", description = "${column.comment}")
    private Date updateTime;

}

