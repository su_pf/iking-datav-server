package com.ikingtech.platform.datav.component.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Date;
import lombok.Data;

@Schema
@Data
@TableName(value = "datav_template_component")
public class TemplateComponent {
    @TableId(value = "id", type = IdType.INPUT)
    @Schema(description="")
    private String id;

    /**
     * 大屏ID
     */
    @TableField(value = "screen_id")
    @Schema(description="大屏ID")
    private String screenId;

    /**
     * 排序序号
     */
    @TableField(value = "sort_order")
    @Schema(description="排序序号")
    private Integer sortOrder;

    /**
     * 类型
     */
    @TableField(value = "`type`")
    @Schema(description="类型")
    private String type;

    /**
     * 别名
     */
    @TableField(value = "`alias`")
    @Schema(description="别名")
    private String alias;

    /**
     * 名称
     */
    @TableField(value = "`name`")
    @Schema(description="名称")
    private String name;

    /**
     * 子组件
     */
    @TableField(value = "children")
    @Schema(description="子组件")
    private String children;

    /**
     * 是否锁定
     */
    @TableField(value = "locked")
    @Schema(description="是否锁定")
    private Byte locked;

    /**
     * 父级ID
     */
    @TableField(value = "parent_id")
    @Schema(description="父级ID")
    private String parentId;

    /**
     * 是否隐藏
     */
    @TableField(value = "hided")
    @Schema(description="是否隐藏")
    private Byte hided;

    /**
     * 图标
     */
    @TableField(value = "icon")
    @Schema(description="图标")
    private String icon;

    /**
     * 图片
     */
    @TableField(value = "img")
    @Schema(description="图片")
    private String img;

    /**
     * 属性
     */
    @TableField(value = "attr")
    @Schema(description="属性")
    private String attr;

    /**
     * 配置
     */
    @TableField(value = "config")
    @Schema(description="配置")
    private String config;

    /**
     * API配置
     */
    @TableField(value = "apis")
    @Schema(description="API配置")
    private String apis;

    /**
     * API数据
     */
    @TableField(value = "api_data")
    @Schema(description="API数据")
    private String apiData;

    /**
     * 入场动画
     */
    @TableField(value = "animate")
    @Schema(description="入场动画")
    private String animate;

    /**
     * 背景图
     */
    @TableField(value = "bg_img")
    @Schema(description="背景图")
    private String bgImg;

    /**
     * 组内配置
     */
    @TableField(value = "scaling")
    @Schema(description="组内配置")
    private String scaling;

    /**
     * 事件
     */
    @TableField(value = "events")
    @Schema(description="事件")
    private String events;

    /**
     * 是否选中
     */
    @TableField(value = "selected")
    @Schema(description="是否选中")
    private Byte selected;

    /**
     * 是否悬停
     */
    @TableField(value = "hovered")
    @Schema(description="是否悬停")
    private Byte hovered;

    /**
     * 背景动画
     */
    @TableField(value = "bg_animation")
    @Schema(description="背景动画")
    private String bgAnimation;

    /**
     * 弹窗配置
     */
    @TableField(value = "dialog")
    @Schema(description="弹窗配置")
    private String dialog;

    /**
     * 是否为弹窗
     */
    @TableField(value = "is_dialog")
    @Schema(description="是否为弹窗")
    private Integer isDialog;

    /**
     * 版本
     */
    @TableField(value = "version")
    @Schema(description="版本")
    private Integer version;

    /**
     * 模板ID
     */
    @TableField(value = "template_id")
    @Schema(description="模板ID")
    private String templateId;

    @TableField(value = "create_time")
    @Schema(description="")
    private Date createTime;

    @TableField(value = "update_time")
    @Schema(description="")
    private Date updateTime;
}