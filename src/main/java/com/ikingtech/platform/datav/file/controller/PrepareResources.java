package com.ikingtech.platform.datav.file.controller;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.oss.model.OssFileDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.datav.config.FileConstant;
import com.ikingtech.platform.datav.systemconfig.entity.SystemConfigDO;
import com.ikingtech.platform.datav.systemconfig.service.SystemConfigService;
import com.ikingtech.platform.service.oss.controller.OssFileController;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * created on 2024-03-22 13:46
 * 准备资源
 *
 * @author wub
 */

@Slf4j
@Component
@RequiredArgsConstructor
public class PrepareResources implements ApplicationListener<ApplicationReadyEvent> {

    private final OssFileController ossApi;

    private final StringRedisTemplate redisTemplate;

    private final SystemConfigService systemConfigService;

    @Value("${datav.fileUrl}")
    public String fileUrl;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {

        List<SystemConfigDO> dataV = systemConfigService.getInfoByType("dataV");
        if (dataV.isEmpty()) {
            SystemConfigDO systemConfig=new SystemConfigDO();
            HashMap<String, Object> map = new HashMap<>(1);
            map.put("fileUrl",fileUrl);
            systemConfig.setType("dataV");
            systemConfig. setProperties(Tools.Json.toJsonStr(map));
            systemConfigService.save(systemConfig);
            log.info("写入fileUrl资源预览地址：{}", fileUrl);
        }
        //资源初始化KEY
        final String readyComplete = "RESOURCE_READY_COMPLETE";
        log.info("正在检查资源包...");
        Boolean flag = redisTemplate.hasKey(readyComplete);
        if (Boolean.TRUE.equals(flag)) {
            log.info("资源包已初始化");
            return;
        }
        log.info("开始初始化资源..");
        if (this.proceed()) {
            redisTemplate.opsForValue().set(readyComplete, Boolean.TRUE.toString());
            log.info("资源初始化完成!!!");
        }
    }

    private boolean proceed() {
        AtomicBoolean flag = new AtomicBoolean(true);
        try {
            String fileBaseUrl = FileConstant.FILE_BASE_URL;
            String basePath = "static/datav/file";
            HashMap<String, String> map = new LinkedHashMap<>();
            map.put(basePath + "/com-picture", fileBaseUrl + "/com-picture");
            map.put(basePath + "/image", fileBaseUrl + "/resource/image");
            map.put(basePath + "/mapjson", fileBaseUrl + "/mapjson");
            map.put(basePath + "/system", fileBaseUrl + "/system");
            map.put(basePath + "/template", fileBaseUrl + "/template");
            map.put(basePath, fileBaseUrl);
            map.forEach((k, v) -> {
                Map<String, byte[]> stringMap = this.readDirectoryAsMap(k);
                stringMap.forEach((fileName, bytes) -> {
                    if(Tools.Str.isNotBlank(fileName)){
//                        R<OssFileDTO> oss = ossApi.uploadByte(fileName, v, bytes, true, true);
//                        if (!oss.isSuccess()) {
//                            flag.set(false);
//                        }
                    }
                });
            });
        } catch (Exception e) {
            flag.set(false);
            log.error("资源初始化失败,异常信息{}", e.getMessage());
            e.printStackTrace();
        }
        return flag.get();
    }

    private Map<String, byte[]> readDirectoryAsMap(String directoryPath) {
        Map<String, byte[]> fileDataMap = new HashMap<>(4);
        try {
            PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource[] resources = resolver.getResources("classpath*:" + directoryPath + "/*.*");
            for (Resource resource : resources) {
                InputStream inputStream = resource.getInputStream();
                byte[] fileBytes = FileCopyUtils.copyToByteArray(inputStream);
                fileDataMap.put(resource.getFilename(), fileBytes);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileDataMap;
    }
}
