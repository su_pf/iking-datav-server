package com.ikingtech.platform.datav.info.model;

import lombok.Data;

@Data
public class InfoDeleteDTO {

    private String ids;
}
