package com.ikingtech.platform.datav.filter.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author fucb
 */

@RequiredArgsConstructor
public enum FilterExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 过滤器不存在
     */
    FILTER_NOT_FOUND("filterNotFound"),
    ;

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-maintenace-datav";
    }
}
