package com.ikingtech.platform.datav.filter.model.enums;

import lombok.RequiredArgsConstructor;

/**
 * @author fucb
 */
@RequiredArgsConstructor
public enum FilterTypeEnum {

    /**
     * 普通过滤器
     */
    COMMON("普通过滤器"),

    /**
     * 模板过滤器
     */
    TEMPLATE("模板过滤器"),
    ;

    public final String value;

}
