package com.ikingtech.platform.datav.template.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 模板表(TemplateDO)实体类
 *
 * @author fucb
 * @since 2024-02-22 10:56:52
 */

@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "datav_template")
public class TemplateDO extends BaseEntity {

    /**
     * 描述
     */
    @TableField("description")
    private String description;

    /**
     * 配置
     */
    @TableField("config")
    private String config;

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 宽度
     */
    @TableField("width")
    private Integer width;

    /**
     * 高度
     */
    @TableField("height")
    private Integer height;

    /**
     * 快照
     */
    @TableField("snapshot")
    private String snapshot;

    /**
     * 组件
     */
    @TableField("coms")
    private String coms;

    /**
     * 描述
     */
    @TableField("is_system")
    private Boolean isSystem;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private String userId;

}
