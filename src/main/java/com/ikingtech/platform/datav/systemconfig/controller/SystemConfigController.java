package com.ikingtech.platform.datav.systemconfig.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.text.CharSequenceUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.DeleteRequest;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.platform.datav.systemconfig.entity.SystemConfigDO;
import com.ikingtech.platform.datav.systemconfig.pojo.SystemConfigDTO;
import com.ikingtech.platform.datav.systemconfig.pojo.SystemConfigSearchDTO;
import com.ikingtech.platform.datav.systemconfig.pojo.SystemConfigVO;
import com.ikingtech.platform.datav.systemconfig.service.SystemConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * created on 2024-03-22 14:45
 *
 * @author wub
 */

@RestController
@RequiredArgsConstructor
@ApiController(value = "/system-config", name = "系统配置systemConfig", description = "系统配置systemConfig")
public class SystemConfigController {

    private final SystemConfigService service;

    @PostRequest(value = "/select/page", summary = "查询系统配置表-分页查询", description = "查询系统配置表-分页查询")
    public R<List<SystemConfigVO>> selectByPage(@RequestBody SystemConfigSearchDTO param) {
        return R.ok(this.service.selectByPage(param));
    }

    @GetRequest(value = "/get/info", summary = "查询系统配置表详情", description = "查询系统配置表详情")
    public R<SystemConfigVO> getInfo(@RequestParam("id") String id) {
        return R.ok(this.service.getInfo(id));
    }

    @PostRequest(value = "/add", summary = "新增系统配置表", description = "新增系统配置表")
    public R<String> add(@RequestBody SystemConfigDTO param) {
        this.service.add(param);
        return R.ok();
    }

    @PostRequest(value = "/edit", summary = "编辑系统配置表", description = "编辑系统配置表")
    public R<String> edit(@RequestBody SystemConfigDTO param) {
        this.service.edit(param);
        return R.ok();
    }

    @DeleteRequest(value = "/delete", summary = "删除系统配置表", description = "删除系统配置表")
    public R<String> delete(@RequestParam("id") String id) {
        this.service.delete(id);
        return R.ok();
    }

    @GetRequest(value = "/getFileUrl", summary = "获取dataV配置的文件服务器地址", description = "获取dataV配置的文件服务器地址")
    public R<String> getFileUrl() {
        List<SystemConfigDO> dataV = this.service.getInfoByType("dataV");
        if(dataV.isEmpty()){
            return R.failed(CharSequenceUtil.EMPTY);
        }
        SystemConfigDO systemConfig = dataV.get(0);
        HashMap<String, Object> map = Tools.Json.toBean(systemConfig.getProperties(), new TypeReference<>() {});
        return R.ok(map.getOrDefault("fileUrl",CharSequenceUtil.EMPTY).toString());
    }

    @GetRequest(value = "/by/type", summary = "根据type查询配置", description = "根据type查询配置")
    public R<SystemConfigVO> byType(@RequestParam("type") String type) {
        SystemConfigDO systemConfig = this.service.getInfoByType(type).get(0);
        return R.ok(BeanUtil.copyProperties(systemConfig,SystemConfigVO.class));
    }

}
