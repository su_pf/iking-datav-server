package com.ikingtech.platform.datav.systemconfig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.datav.systemconfig.entity.SystemConfigDO;

/**
 * created on 2024-03-22 14:42
 *
 * @author wub
 */

public interface SystemConfigMapper extends BaseMapper<SystemConfigDO> {
}
